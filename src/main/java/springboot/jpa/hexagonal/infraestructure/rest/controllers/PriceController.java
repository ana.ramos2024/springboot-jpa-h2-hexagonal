package springboot.jpa.hexagonal.infraestructure.rest.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import springboot.jpa.hexagonal.application.services.PriceService;
import springboot.jpa.hexagonal.domain.models.PriceDTO;

/**
 * The Class PriceController.
 */
// Controlador REST para exponer el punto final de consulta
@RestController
@RequestMapping("/prices")
public class PriceController {
    
    /** The price service. */
    @Autowired
    private PriceService priceService;
    
	/**
	 * Gets the price.
	 *
	 * @param brandId   the brand id
	 * @param productId the product id
	 * @param date      the start date
	 * @return the price
	 * @throws JsonProcessingException 
	 */
	@GetMapping
	public String getPrice(

			@RequestParam(required = false) Long brandId, @RequestParam(required = false) Long productId,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date startDate) throws JsonProcessingException {
		List<PriceDTO> lstPrice = priceService.getPrice(brandId, productId, startDate);
		if (lstPrice != null) {
			// Convertir la lista a formato JSON
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(lstPrice);
		} else {
			return null;
		}
	}
    
}
