package springboot.jpa.hexagonal.infraestructure.adapters.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * The Class Price.
 */
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
@Entity
@Table(name = "prices")
public class PriceEntity {
	
	/** The id. */
	@Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long id;
	
	/** The brand id. */
	@Column(name = "brand_id")
	private Long brandId;

	/** The start date. */
	@Column(name = "start_date")
	private Date startDate;
	
	/** The end date. */
	@Column(name = "end_date")
	private Date endDate;
	
	/** The price list. */
	@Column(name = "price_list")
	private Long priceList;
	
	/** The product id. */
	@Column(name = "product_id")
	private Long productId;
	
	/** The priority. */
	@Column(name = "priority")
	private Integer priority;
	
	/** The price. */
	@Column(name = "price")
	private BigDecimal price;
	
	/** The currency. */
	@Column(name = "currency")
	private String currency;
	
	/** The last update. */
	@Column(name = "last_update")
	private Date lastUpdate;
	
	/** The last update by. */
	@Column(name = "last_update_by")
	private String lastUpdateBy;
	
	/* CREATE TABLE PRICES (
		    id BIGINT AUTO_INCREMENT PRIMARY KEY,
		    brandId BIGINT NOT NULL,
		    startDate DATETIME NOT NULL,
		    endDate DATETIME NOT NULL,
		    priceList BIGINT NOT NULL,
		    productId BIGINT NOT NULL,
		    priority INT NOT NULL,
		    price DECIMAL(19, 2) NOT NULL,
		    currency VARCHAR(3) NOT NULL
		);*/

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the brand id.
	 *
	 * @return the brand id
	 */
	public Long getBrandId() {
		return brandId;
	}

	/**
	 * Sets the brand id.
	 *
	 * @param brandId the new brand id
	 */
	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Sets the start date.
	 *
	 * @param startDate the new start date
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the price list.
	 *
	 * @return the price list
	 */
	public Long getPriceList() {
		return priceList;
	}

	/**
	 * Sets the price list.
	 *
	 * @param priceList the new price list
	 */
	public void setPriceList(Long priceList) {
		this.priceList = priceList;
	}

	/**
	 * Gets the product id.
	 *
	 * @return the product id
	 */
	public Long getProductId() {
		return productId;
	}

	/**
	 * Sets the product id.
	 *
	 * @param productId the new product id
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}

	/**
	 * Gets the priority.
	 *
	 * @return the priority
	 */
	public Integer getPriority() {
		return priority;
	}

	/**
	 * Sets the priority.
	 *
	 * @param priority the new priority
	 */
	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * Sets the price.
	 *
	 * @param price the new price
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * Gets the last update.
	 *
	 * @return the last update
	 */
	public Date getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * Sets the last update.
	 *
	 * @param lastUpdate the new last update
	 */
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	/**
	 * Gets the last update by.
	 *
	 * @return the last update by
	 */
	public String getLastUpdateBy() {
		return lastUpdateBy;
	}

	/**
	 * Sets the last update by.
	 *
	 * @param lastUpdateBy the new last update by
	 */
	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}
	}