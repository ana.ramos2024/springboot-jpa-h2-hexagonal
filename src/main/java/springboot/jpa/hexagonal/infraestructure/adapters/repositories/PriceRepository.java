package springboot.jpa.hexagonal.infraestructure.adapters.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import springboot.jpa.hexagonal.infraestructure.adapters.entity.PriceEntity;


/**
 * The Interface PriceRepository.
 */
// Repositorio para acceder a los datos de la tabla PRICES
@Repository
public interface PriceRepository extends JpaRepository<PriceEntity, Long> {

	/**
	 * Find by brand id and product id and start date.
	 *
	 * @param brandId the brand id
	 * @param productId the product id
	 * @param startDate the start date
	 * @return the list
	 */
	@Query("SELECT p.productId, p.brandId, p.priority, p.currency, (SELECT MAX(p.price)) as price, "
			+ "(SELECT MAX(CASE WHEN p.startDate > p.endDate THEN p.startDate ELSE p.endDate END) FROM PriceEntity p) as date FROM PriceEntity p "
			+ "WHERE p.brandId = :brandId AND p.productId = :productId AND :startDate BETWEEN p.startDate AND p.endDate")
	List<Object[]> findByBrandIdAndProductIdAndStartDate(@Param("brandId") Long brandId,
			@Param("productId") Long productId, @Param("startDate") Date startDate);

	/**
	 * Find by brand id and product id.
	 *
	 * @param brandId the brand id
	 * @param productId the product id
	 * @return the list
	 */
	@Query("SELECT p.productId, p.brandId, p.priority, p.currency, (SELECT MAX(p.price)) as price, "
			+ "(SELECT MAX(CASE WHEN p.startDate > p.endDate THEN p.startDate ELSE p.endDate END) FROM PriceEntity p) as date FROM PriceEntity p "
			+ "WHERE p.brandId = :brandId AND p.productId = :productId AND (SELECT MAX(CASE WHEN p.startDate > p.endDate THEN p.startDate ELSE p.endDate END) FROM PriceEntity p)"
			+ "BETWEEN p.startDate AND p.endDate")
	List<Object[]> findByBrandIdAndProductId(@Param("brandId") Long brandId, @Param("productId") Long productId);

	/**
	 * Find by brand id and start date.
	 *
	 * @param brandId the brand id
	 * @param startDate the start date
	 * @return the list
	 */
	@Query("SELECT p.productId, p.brandId, p.priority, p.currency, (SELECT MAX(p.price)) as price, "
			+ "(SELECT MAX(CASE WHEN p.startDate > p.endDate THEN p.startDate ELSE p.endDate END) FROM PriceEntity p) as date FROM PriceEntity p "
			+ "WHERE p.brandId = :brandId AND :startDate BETWEEN p.startDate AND p.endDate")
	List<Object[]> findByBrandIdAndStartDate(@Param("brandId") Long brandId, @Param("startDate") Date startDate);

	/**
	 * Find by product id and start date.
	 *
	 * @param productId the product id
	 * @param startDate the start date
	 * @return the list
	 */
	//"SELECT MAX(CASE WHEN p.startDate > p.endDate THEN p.startDate ELSE p.endDateEND) FROM PriceEntity e";
    
	@Query("SELECT p.productId, p.brandId, p.priority, p.currency, (SELECT MAX(p.price)) as price, "
			+ "(SELECT MAX(CASE WHEN p.startDate > p.endDate THEN p.startDate ELSE p.endDate END) FROM PriceEntity p) as date FROM PriceEntity p "
			+ "WHERE p.productId = :productId AND :startDate BETWEEN p.startDate AND p.endDate")
	List<Object[]> findByProductIdAndStartDate(@Param("productId") Long productId,
			@Param("startDate") Date startDate);

	/**
	 * Find by product id.
	 *
	 * @param productId the product id
	 * @return the list
	 */
	@Query("SELECT p.productId, p.brandId, p.priority, p.currency, MAX(p.price) as price, "
			+ "(SELECT MAX(CASE WHEN p.startDate > p.endDate THEN p.startDate ELSE p.endDate END) FROM PriceEntity p) as date FROM PriceEntity p "
			+"WHERE p.productId = :productId AND (SELECT MAX(CASE WHEN p.startDate > p.endDate THEN p.startDate ELSE p.endDate END) FROM PriceEntity p) BETWEEN p.startDate AND p.endDate "
			+ "GROUP BY p.productId, p.brandId, p.priority, p.currency")
	List<Object[]> findByProductId(@Param("productId") Long productId);

	/**
	 * Find by start date.
	 *
	 * @param startDate the start date
	 * @return the list
	 */
	@Query("SELECT p.productId, p.brandId, p.priority, p.currency, p.price,"
			+ "(SELECT MAX(CASE WHEN p.startDate > p.endDate THEN p.startDate ELSE p.endDate END) FROM PriceEntity p) as date FROM PriceEntity p "
			+ "WHERE :startDate BETWEEN p.startDate AND p.endDate AND (SELECT MAX(CASE WHEN p.startDate > p.endDate THEN p.startDate ELSE p.endDate END) FROM PriceEntity p) BETWEEN p.startDate AND p.endDate"
			+ " ")
	List<Object[]> findByStartDate(@Param("startDate") Date startDate);

	/**
	 * Find by brand id.
	 *
	 * @param brandId the brand id
	 * @return the list
	 */
	@Query("SELECT p.productId, p.brandId, p.priority, p.currency, (SELECT MAX(p.price)) as price, "
			+ "(SELECT MAX(CASE WHEN p.startDate > p.endDate THEN p.startDate ELSE p.endDate END) FROM PriceEntity p) as date FROM PriceEntity p "
			+ "WHERE p.brandId = :brandId AND (SELECT MAX(CASE WHEN p.startDate > p.endDate THEN p.startDate ELSE p.endDate END) "
			+ "FROM PriceEntity p) BETWEEN p.startDate AND p.endDate")
	List<Object[]> findByBrandId(@Param("brandId") Long brandId);

}
