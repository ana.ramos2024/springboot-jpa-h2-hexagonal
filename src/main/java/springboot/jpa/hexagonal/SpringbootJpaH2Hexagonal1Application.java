package springboot.jpa.hexagonal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootJpaH2Hexagonal1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootJpaH2Hexagonal1Application.class, args);
	}

}
