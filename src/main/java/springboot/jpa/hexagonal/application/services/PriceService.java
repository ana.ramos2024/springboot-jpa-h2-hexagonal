package springboot.jpa.hexagonal.application.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import springboot.jpa.hexagonal.application.mapper.PriceDTOMapper;
import springboot.jpa.hexagonal.domain.models.PriceDTO;
import springboot.jpa.hexagonal.infraestructure.adapters.repositories.PriceRepository;

/**
 * The Class PriceService.
 */
@Service
public class PriceService {

	/** The price repository. */

	private final PriceRepository priceRepository;

	public PriceService(PriceRepository priceRepository) {
		this.priceRepository = priceRepository;
	}

	/**
	 * Gets the price.
	 *
	 * @param brandId   the brand id
	 * @param productId the product id
	 * @param date      the date
	 * @return the price
	 */
	public List<PriceDTO> getPrice(Long brandId, Long productId, Date date) {
		List<Object[]> prices = new ArrayList<>();
		if (brandId == null && productId == null && date == null) {// ok
			// Si no se proporciona ningún parámetro de búsqueda, retornar null
			return null;
		
		} else if (brandId != null && productId != null && date != null) {// ok
			// Si se proporcionan todos los parámetros de búsqueda, buscar por todos
			prices = priceRepository.findByBrandIdAndProductIdAndStartDate(brandId, productId, date);
		
		} else if (brandId != null && productId != null) {//ok
			// Si se proporcionan brandId y productId, buscar por ambos
			prices = priceRepository.findByBrandIdAndProductId(brandId, productId);

		} else if (productId != null && date != null) {// ok
			// Si se proporcionan productId y date, buscar por productId y fecha
			prices = priceRepository.findByProductIdAndStartDate(productId, date);
		
		} else if (brandId != null && date != null) {// ok
			// Si se proporcionan brandId y date, buscar por brandId y fecha
			prices = priceRepository.findByBrandIdAndStartDate(brandId, date);
		
		} else if (productId != null) {
			// Si se proporciona solo productId, buscar por productId
			prices = priceRepository.findByProductId(productId);
		
		} else if (date != null) {// ok
			// Si se proporciona solo la fecha, buscar por fecha
			prices = priceRepository.findByStartDate(date);
		
		} else if (brandId != null) {
			// Si se proporciona solo brandId, buscar por brandId
			prices = priceRepository.findByBrandId(brandId);
		}

		return convertToObjectList(prices);
	}

	// Método para convertir una lista de arrays de objetos en una lista de
	// entidades Price
	private List<PriceDTO> convertToObjectList(List<Object[]> results) {
		List<PriceDTO> prices = new ArrayList<>();
		for (Object[] row : results) {
			PriceDTO price = new PriceDTO();
			price.setProductId((Long) row[0]);
			price.setBrandId((Long) row[1]);
			price.setPriority((Integer) row[2]);
			price.setCurrency((String) row[3]);
			price.setPrice((BigDecimal) row[4]);
			prices.add(price);
		}
		return prices;
	}
}