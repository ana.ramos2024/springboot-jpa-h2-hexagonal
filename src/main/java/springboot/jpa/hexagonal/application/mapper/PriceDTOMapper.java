package springboot.jpa.hexagonal.application.mapper;

import springboot.jpa.hexagonal.domain.models.PriceDTO;

/**
 * The Class PriceDTOMapper.
 */
public interface PriceDTOMapper {
	PriceDTO dto(PriceDTO domain);
	
	
}