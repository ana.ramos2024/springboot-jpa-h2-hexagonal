package test.springboot.jpa.hexagonal.service;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import springboot.jpa.hexagonal.application.services.PriceService;
import springboot.jpa.hexagonal.domain.models.PriceDTO;
import springboot.jpa.hexagonal.infraestructure.adapters.repositories.PriceRepository;

/**
 * The Class PriceServiceTest.
 */
public class PriceServiceTest {

    /** The price repository. */
    private PriceRepository priceRepository;
    
    /** The price service. */
    private PriceService priceService;

    /**
     * Sets the up.
     */
    @Before
    public void setUp() {
        priceRepository = mock(PriceRepository.class);
        priceService = new PriceService(priceRepository);
    }

    /**
     * Test get price all parameters provided.
     *
     * @throws ParseException the parse exception
     */
    @Test
    public void testGetPrice_AllParametersProvided() throws ParseException {
    	Long brandId = 1L;
        Long productId = 35455L;
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        List<PriceDTO> result = testGetPrice_Service(brandId, productId, dateFormat.parse("2020-06-14 10:00:00"));
        testGetPrice_Service(brandId, productId, dateFormat.parse("2020-06-14 16:00:00"));
        testGetPrice_Service(brandId, productId, dateFormat.parse("2020-06-14 21:00:00"));
        testGetPrice_Service(brandId, productId, dateFormat.parse("2020-06-15 16:00:00"));
        testGetPrice_Service(brandId, productId, dateFormat.parse("2020-06-15 21:00:00"));
        
     // Verificación de los resultados
        assertEquals(5, result.size());
        assertEquals(productId, (long) result.get(0).getProductId());
        assertEquals(1L, (long) result.get(0).getBrandId());
        assertEquals(0, (int) result.get(0).getPriority());
        assertEquals("EUR", result.get(0).getCurrency());
        assertNotEquals(BigDecimal.valueOf(10.0), result.get(0).getPrice());

    }
    
    /**
     * Test get price service.
     *
     * @param brandId the brand id
     * @param productId the product id
     * @param date the date
     * @return the list
     * @throws ParseException the parse exception
     */
    public List<PriceDTO> testGetPrice_Service(Long brandId, Long productId, Date date) throws ParseException {
    	
    	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
    	// Mock de los resultados del repositorio
        List<Object[]> mockResults = new ArrayList<>();
        mockResults.add(new Object[] {productId, brandId, 0, "EUR", BigDecimal.valueOf(35.50) });
        mockResults.add(new Object[] {productId, brandId, 0, "EUR", BigDecimal.valueOf(15.50) });
        mockResults.add(new Object[] {productId, brandId, 0, "EUR", BigDecimal.valueOf(5.50) });
        mockResults.add(new Object[] {productId, brandId, 0, "EUR", BigDecimal.valueOf(5.50) });
        mockResults.add(new Object[] {productId, brandId, 0, "EUR", BigDecimal.valueOf(35.50) });
        when(priceRepository.findByBrandIdAndProductIdAndStartDate(brandId, productId, dateFormat.parse("2020-06-14 10:00:00"))).thenReturn(mockResults);

    	
        // Llamada al método bajo prueba
        return priceService.getPrice(brandId, productId, date);

        
    }
}
