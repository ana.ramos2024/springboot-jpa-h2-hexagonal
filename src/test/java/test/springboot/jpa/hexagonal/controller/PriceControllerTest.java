package test.springboot.jpa.hexagonal.controller;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import springboot.jpa.hexagonal.application.services.PriceService;
import springboot.jpa.hexagonal.domain.models.PriceDTO;
import springboot.jpa.hexagonal.infraestructure.rest.controllers.PriceController;


/**
 * The Class PriceControllerTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class PriceControllerTest {

    /** The price service. */
    @Mock
    private PriceService priceService;

    /** The price controller. */
    @InjectMocks
    private PriceController priceController;

    /**
     * Setup.
     */
    @Before
    public void setup() {
    	MockitoAnnotations.initMocks(this);
    }

    /**
     * Test get price.
     *
     * @throws JsonProcessingException the json processing exception
     */
    @Test
    public void testGetPrice() throws JsonProcessingException {
        // Datos de ejemplo
        List<PriceDTO> mockPrices = new ArrayList<>();
        mockPrices.add(new PriceDTO());
        mockPrices.add(new PriceDTO());

        // Mockear el comportamiento del servicio
        when(priceService.getPrice(anyLong(), anyLong(), any(Date.class))).thenReturn(mockPrices);

        // Llama al método del controlador
        String result = priceController.getPrice(1L, 2L, new Date());

        // Verifica si el resultado es lo esperado
        ObjectMapper mapper = new ObjectMapper();
        String expectedResult = mapper.writeValueAsString(mockPrices);
        assertEquals(expectedResult, result);
    }

    /**
     * Test get price null.
     *
     * @throws JsonProcessingException the json processing exception
     */
    @Test
    public void testGetPrice_Null() throws JsonProcessingException {
        // Mockear el servicio para devolver null
        when(priceService.getPrice(anyLong(), anyLong(), any(Date.class))).thenReturn(null);

        // Llama al método del controlador
        String result = priceController.getPrice(1L, 2L, new Date());

        // Verifica si el resultado es null
        assertNull(result);
    }
    
    /**
     * Test get price not found.
     *
     * @throws JsonProcessingException the json processing exception
     */
    @Test
    public void testGetPrice_NotFound() throws JsonProcessingException {
        // Mockear el servicio para devolver una lista vacía (not found)
        when(priceService.getPrice(anyLong(), anyLong(), any(Date.class))).thenReturn(new ArrayList<>());

        // Llama al método del controlador
        String result = priceController.getPrice(1L, 2L, new Date());

        // Verifica si el resultado es una lista vacía en formato JSON
        assertEquals("[]", result);
    }

}
