## Test hexagonal, JPA and H2

    Este proyecto es un ejemplo práctico del desarrollo de una API REST siguiendo una Arquitectura Hexagonal, utilizando H2 en base de datos en memoria.

## Tecnologías necesarias
   Java, Maven, JPA, H2, hibernate


## Instalacion del proyecto 


 1. Clonar el repositorio en tu equipo:

    cd <folder path>
    https://gitlab.com/ana.ramos2024/springboot-jpa-h2-hexagonal
    
 2. Importar el proyecto mediante Spring Boot

    Import Project, y seleccionar la carpeta del proyecto.
    
## Configuraciòn

  1. application.properties
  
  	spring.datasource.url=jdbc:h2:mem:test
	spring.datasource.driverClassName=org.h2.Driver
	spring.datasource.username=sa
	spring.datasource.password=
	spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
	spring.jpa.defer-datasource-initialization=true
	spring.h2.console.enabled=true
	spring.jpa.show-sql=true
	spring.jpa.hibernate.ddl-auto=update
  
  2. data.sql
  	
  	INSERT INTO prices (brand_id, start_date, end_date, price_list, product_id, priority, price, currency, last_update_by, last_update)
	VALUES
	(1, '2020-06-14 10:00:00', '2020-12-31 23:59:59', 1, 35455, 0, 35.50, 'EUR', 'sa', now()),
	(1, '2020-06-14 15:00:00', '2020-06-14 18:30:00', 2, 35455, 1, 25.45, 'EUR', 'sa', now()),
	(1, '2020-06-15 00:00:00', '2020-06-15 11:00:00', 3, 35455, 1, 30.50, 'EUR', 'sa', now()),
	(1, '2020-06-15 16:00:00', '2020-12-31 23:59:59', 4, 35455, 1, 38.95, 'EUR', 'sa', now());


## Ejecución

    Desde Spring Boot, clic derecho sobre el proyecto y selecciona la opcion Run as > "Spring Boot App"

## Testing

    Ejecutar tests unitarios: > mvn test
    Desde Spring Boot, clic derecho sobre el proyecto y selecciona la opcion Run as JUnit Test" 

  


